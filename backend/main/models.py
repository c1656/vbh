from django.db import models
from ckeditor.fields import RichTextField

class Company(models.Model):
    name = models.CharField(max_length=200)
    location = models.CharField(max_length=200)
    website = models.URLField(max_length=200, blank=True, null=True)
    phone = models.TextField()

    class Meta:
        verbose_name = "Компанія"
        verbose_name_plural = "Компанії"
        
    def __str__(self):
        return self.name

class Vacancy(models.Model):
    title = models.CharField(max_length=200, verbose_name="Назва")
    job_summary = RichTextField(blank=True, null=True, verbose_name="Опис")
    description = RichTextField(blank=True, null=True, verbose_name="Обов'язки") 
    requirements = RichTextField(blank=True, null=True, verbose_name="Вимоги")  
    benefits = RichTextField(blank=True, null=True, verbose_name="Переваги")
    salary = models.TextField(blank=True, null=True, verbose_name="Зарплата")
    is_active = models.BooleanField(default=True)
    posted_on = models.DateField(auto_now_add=True)
    company = models.ForeignKey(Company, on_delete=models.CASCADE, related_name='vacancies', verbose_name="Компанія")

    class Meta:
        verbose_name = "Вакансія"
        verbose_name_plural = "Вакансії"
    
    def __str__(self):
        return f"{self.title} at {self.company.name}"

