from django.contrib import admin
from .models import Company, Vacancy
from ckeditor.widgets import CKEditorWidget
from django import forms

class CompanyAdmin(admin.ModelAdmin):
    list_display = ('name', 'location', 'website')
    search_fields = ('name', 'location')

class VacancyAdminForm(forms.ModelForm):
    description = forms.CharField(widget=CKEditorWidget(), label="Обов'язки")
    requirements = forms.CharField(widget=CKEditorWidget(), label="Вимоги")
    benefits = forms.CharField(widget=CKEditorWidget(), label="Переваги")
    job_summary = forms.CharField(widget=CKEditorWidget(), label="Опис")

    class Meta:
        model = Vacancy
        fields = '__all__'
        labels = {
            'title': 'Назва',
            'description': 'Опис',
            'requirements': 'Вимоги',
            'benefits': 'Переваги',
            'job_summary': 'Загальний огляд роботи',
            'salary': 'Зарплата',
            'is_active': 'Активна',
            'posted_on': 'Дата публікації',
            'company': 'Компанія',
        }

class VacancyAdmin(admin.ModelAdmin):
    form = VacancyAdminForm
    list_display = ('title', 'company', 'salary', 'is_active', 'posted_on')
    list_filter = ('is_active', 'posted_on', 'company')
    search_fields = ('title', 'description')
    date_hierarchy = 'posted_on'

# Register your models with admin customization
admin.site.register(Company, CompanyAdmin)
admin.site.register(Vacancy, VacancyAdmin)