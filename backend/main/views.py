import os
import requests
import json
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods
from django.shortcuts import render
from rest_framework import generics
from .models import Vacancy
from .serializers import VacancySerializer

class VacancyList(generics.ListCreateAPIView):
    queryset = Vacancy.objects.all()
    serializer_class = VacancySerializer

class VacancyDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Vacancy.objects.all()
    serializer_class = VacancySerializer

@csrf_exempt
@require_http_methods(["POST"])
def send_to_telegram(request):
    # Parse JSON data from request body
    try:
        data = json.loads(request.body)
        name = data.get('name')
        tel = data.get('tel')
        message = data.get('message')
    except json.JSONDecodeError:
        return JsonResponse({'status': 'error', 'message': 'Invalid JSON'}, status=400)

    if not all([name, tel, message]):  # Check if any field is None or empty
        return JsonResponse({'status': 'error', 'message': 'Missing data fields'}, status=400)

    # Prepare the message
    text = f"Ім'я: {name}\nНомер: {tel}\nПовідомлення: {message}"

    # Retrieve environment variables
    token = os.getenv('TELEGRAM_BOT_TOKEN')
    chat_id = os.getenv('CHAT_ID')

    # Telegram API URL
    url = f"https://api.telegram.org/bot{token}/sendMessage"

    # Send message to Telegram
    response = requests.post(url, data={'chat_id': chat_id, 'text': text})
    if response.status_code == 200:
        return JsonResponse({'status': 'success', 'message': 'Message sent to Telegram'})
    else:
        return JsonResponse({'status': 'error', 'message': 'Failed to send message to Telegram', 'telegram_response': response.text}, status=500)
    