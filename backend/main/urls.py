from django.urls import path 
from .views import VacancyList, VacancyDetail, send_to_telegram

urlpatterns = [
    path('vacancies/', VacancyList.as_view(), name='vacancy-list'),
    path('vacancy/<int:pk>/', VacancyDetail.as_view(), name='vacancy-detail'),
    path('send-to-telegram/', send_to_telegram, name='send-to-telegram'),
]
