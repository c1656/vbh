from rest_framework import serializers # type: ignore
from .models import Vacancy, Company

class CompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = ['id', 'name', 'location', 'website', 'phone']

class VacancySerializer(serializers.ModelSerializer):
    company = CompanySerializer(read_only=True)
    
    class Meta:
        model = Vacancy
        fields = '__all__'