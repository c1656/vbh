import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import "./style.scss";
import Vacancy from "./Vacansy/Vacancy";

export default function Vacancies() {
  const [vacancies, setVacancies] = useState([]);
  const navigate = useNavigate();
  const url = import.meta.env.VITE_BACKEND_URL;

  const hanldeNavigate = (id) => {
    navigate(`/vacancy/${id}`);
  };

  useEffect(() => {
    const fetchVacancies = async () => {
      try {
        const response = await axios.get(`${url}/api/v1/vacancies`);
        setVacancies(response.data);
      } catch (error) {
        console.error("Error fetching data: ", error);
      }
    };

    fetchVacancies();
  }, []);

  return vacancies.length > 0 ? (
    <section className="vacancies">
      <div className="container">
        <h2>ВАКАНСІЇ</h2>
        {vacancies.map((vacancy) => (
          <Vacancy
            key={vacancy.id}
            title={vacancy.title}
            salary={vacancy.salary}
            job_summary={vacancy.job_summary}
            company={vacancy.company.name}
            onClick={() => hanldeNavigate(vacancy.id)}
          />
        ))}
      </div>
    </section>
  ) : (
    <section className="vacancies">
      <div className="container">
        <h1>Незабаром тут зявляться вакансії</h1>
      </div>
    </section>
  );
}
