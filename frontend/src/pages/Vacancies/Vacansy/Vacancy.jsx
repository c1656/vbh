import "./style.scss";

export default function Vacancy({
  key,
  title,
  salary,
  company,
  job_summary,
  onClick,
}) {
  return (
    <section onClick={onClick} className="vacancy" key={key}>
      <h3>{title}</h3>
      <span className="salary">{salary} грн</span>
      <span className="company">{company}</span>
      <p
        className="description"
        dangerouslySetInnerHTML={{ __html: job_summary }}
      />
    </section>
  );
}
