import {
  Main,
  About,
  Problems,
  Directions,
  Form,
  Partners,
} from "../../components";

export default function Home() {
  return (
    <>
      <Main />
      <About />
      <Problems />
      <Directions />
      <Form />
      <Partners />
    </>
  );
}
