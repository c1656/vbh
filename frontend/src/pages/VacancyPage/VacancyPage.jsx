import { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";
import "./style.scss";

export default function VacancyPage() {
  const [vacancy, setVacancy] = useState(null);
  const { id } = useParams();
  const url = import.meta.env.VITE_BACKEND_URL;
  useEffect(() => {
    const fetchVacancies = async () => {
      try {
        const response = await axios.get(`${url}/api/v1/vacancy/${id}`);
        setVacancy(response.data);
      } catch (error) {
        console.error("Error fetching data: ", error);
      }
    };

    fetchVacancies();
  }, [id]);

  return (
    vacancy != null && (
      <section className="vacancy-page">
        <div className="container">
          <h1>{vacancy.title}</h1>
          <p className="salary">{vacancy.salary} грн</p>
          <p className="company">{vacancy.company.name}</p>
          <div className="wrapper">
            <span>Контакти:</span>
            <br />

            {vacancy.company.website && (
              <a href={vacancy.company.website}>{vacancy.company.website}</a>
            )}
            {vacancy.company.location && (
              <p className="location">{vacancy.company.location}</p>
            )}
            {vacancy.company.phone && (
              <a href={`tel:${vacancy.company.phone}`} className="phone">
                {vacancy.company.phone}{" "}
              </a>
            )}
          </div>

          {vacancy.description && (
            <div className="description">
              <h5>Опис роботи:</h5>
              <div dangerouslySetInnerHTML={{ __html: vacancy.job_summary }} />
            </div>
          )}
          {vacancy.job_summary && (
            <div className="description">
              <h5>Обов’язки:</h5>
              <div dangerouslySetInnerHTML={{ __html: vacancy.description }} />
            </div>
          )}
          {vacancy.requirements && (
            <div className="description">
              <h5>Вимоги до кандидата:</h5>
              <div dangerouslySetInnerHTML={{ __html: vacancy.requirements }} />
            </div>
          )}
          {vacancy.benefits && (
            <div className="description">
              <h5>Бенефіти:</h5>
              <div dangerouslySetInnerHTML={{ __html: vacancy.benefits }} />
            </div>
          )}
        </div>
      </section>
    )
  );
}
