import "./style.scss";

import inst from "../../assets/icons/instagram.svg";
import tg from "../../assets/icons/telegram.svg";
import fb from "../../assets/icons/facebook.svg";
import mail from "../../assets/icons/mail.png";
import lin from "../../assets/icons/in.png";

export default function Main() {
  return (
    <main>
      <div className="container">
        <div className="content-headers">
          <h1>Ветеранський Бізнес Хаб</h1>
          <h3>Робота - як місток у мирне життя</h3>
        </div>
        <div className="social-media">
          <a target="_blank" href="https://www.instagram.com/veteranbizhub/">
            <img src={inst} />
          </a>
          <a target="_blank" href="https://t.me/veteranbizhub">
            <img src={tg} />
          </a>
          <a
            target="_blank"
            href="https://www.facebook.com/people/%D0%92%D0%B5%D1%82%D0%B5%D1%80%D0%B0%D0%BD%D1%81%D1%8C%D0%BA%D0%B8%D0%B9-%D0%91%D1%96%D0%B7%D0%BD%D0%B5%D1%81-%D0%A5%D0%B0%D0%B1/61552302695064/"
          >
            <img src={fb} />
          </a>
          <a href="mailto:veteranbizhub@gmail.com">
            <img src={mail} />
          </a>
          <a target="_blank" href="https://www.linkedin.com/in/veteranbizhub/">
            <img src={lin} />
          </a>
        </div>
      </div>
    </main>
  );
}
