import React, { useState, useRef } from "react";
import axios from "axios";
import check from "../../assets/icons/circle-check-regular.svg";
import "./style.scss";

export default function Form() {
  const [name, setName] = useState("");
  const [tel, setTel] = useState("");
  const [message, setMessage] = useState("");
  const [submitted, setSubmitted] = useState(false);
  const [errors, setErrors] = useState({});
  const textAreaRef = useRef(null);
  const url = import.meta.env.VITE_BACKEND_URL;

  const handleInput = () => {
    const textarea = textAreaRef.current;
    textarea.style.height = "auto";
    textarea.style.height = `${textarea.scrollHeight}px`;
  };

  const validate = () => {
    const newErrors = {};
    if (!name.trim()) newErrors.name = "Ім'я є обов'язковим";
    if (!tel.trim()) newErrors.tel = "Телефон є обов'язковим";
    if (!message.trim()) newErrors.message = "Повідомлення є обов'язковим";
    return newErrors;
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const newErrors = validate();
    setErrors(newErrors);
    if (Object.keys(newErrors).length > 0) {
      return;
    }

    try {
      await axios.post(
        `${url}/api/v1/send-to-telegram/`,
        {
          name,
          tel,
          message,
        },
        {
          headers: {
            "Content-Type": "application/json",
          },
        }
      );
      setSubmitted(true);
    } catch (error) {
      console.error("Error submitting form:", error);
      console.log(errors);
    }
  };

  return (
    <section className="contacts" id="form">
      <div className="container">
        {submitted ? (
          <div className="successContainer">
            <img id="successLogo" src={check} alt="success logo" />
            <p>Дякуємо. Ваше повідомлення надіслано</p>
          </div>
        ) : (
          <div className="form">
            <form id="myForm" onSubmit={handleSubmit}>
              <h2>Напишіть нам</h2>
              <div className="row">
                <div className="col-12 col-md-6">
                  <input
                    type="text"
                    id="name"
                    name="name"
                    autoComplete="off"
                    placeholder="Ім'я"
                    value={name}
                    onChange={(e) => setName(e.target.value)}
                  />
                  {errors.name && (
                    <div className="error-message">{errors.name}</div>
                  )}
                </div>
                <div className="col-12 col-md-6">
                  <input
                    type="tel"
                    id="tel"
                    name="tel"
                    autoComplete="off"
                    placeholder="Телефон"
                    value={tel}
                    onChange={(e) => setTel(e.target.value)}
                  />
                  {errors.tel && (
                    <div className="error-message">{errors.tel}</div>
                  )}
                </div>
                <div className="col-12">
                  <textarea
                    ref={textAreaRef}
                    placeholder="Повідомлення"
                    rows="1"
                    name="message"
                    style={{ height: "auto" }}
                    value={message}
                    onChange={(e) => {
                      setMessage(e.target.value);
                      handleInput();
                    }}
                  ></textarea>
                  {errors.tel && (
                    <div className="error-message">{errors.tel}</div>
                  )}
                </div>
              </div>
              <button className="form-submit" type="submit">
                Надіслати
              </button>
            </form>
          </div>
        )}
      </div>
    </section>
  );
}
