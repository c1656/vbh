import React, { useState } from "react";
import justment from "../../assets/icons/justment.png";
import carier from "../../assets/icons/carier.png";
import book from "../../assets/icons/book.png";
import "./style.scss";

export default function Directions() {
  const [modalContent, setModalContent] = useState("");
  const [showModal, setShowModal] = useState(false);

  const vectors = [
    {
      imgSrc: justment,
      title: "Юридична підтримка ветеранів та членів їхніх сімей",
      description:
        "Співпраця з юридичними організаціями та скерування запитів по ветеранській тематиці до фахових юристів.;Юридична консультація по правовій частині відкриття бізнесу.",
    },
    {
      imgSrc: carier,
      title: "Працевлаштування ветеранів та професійна консультація",
      description:
        "Допомога ветеранам у знаходженні роботи відповідно до їхніх потреб, інтересів, фізичних та психологічних особливостей.;Співпраця з бізнесом у закритті кадрових потреб шляхом підбору кандидатів на відповідні вакансії для ветеранів.",
    },
    {
      imgSrc: book,
      title: "Гранти та програми для ветеранів та членів їхніх сімей",
      description:
        "Моніторинг та інформування про актуальні грантові програми.;Індивідуальний супровід у подачі заявок на гранти.",
    },
  ];

  const handleVectorClick = (description) => {
    setModalContent(description);
    setShowModal(true);
    console.log(showModal);
  };

  return (
    <section className="main-directions" id="vectors">
      <div className="container">
        <h2>OСНОВНІ НАПРЯМКИ РОБОТИ</h2>
        <div className="vectors">
          {vectors.map((vector, index) => (
            <div
              key={index}
              className="vector"
              onClick={() => handleVectorClick(vector.description)}
            >
              <img src={vector.imgSrc} alt={vector.title} />
              <p className="vector-title">{vector.title}</p>
            </div>
          ))}
        </div>
        {showModal && (
          <div className="modal-backdrop" onClick={() => setShowModal(false)}>
            <div className="modal-content">
              <div className="modal-body">
                {modalContent.split(";").map((item, index) => (
                  <p key={index}>{item}</p>
                ))}
              </div>
            </div>
          </div>
        )}
      </div>
    </section>
  );
}
