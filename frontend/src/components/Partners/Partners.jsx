import "./style.scss";
import arr from "../../assets/logo/arrlogo.png";
import ministry from "../../assets/logo/ministry.png";
import office from "../../assets/logo/office.png";

export default function Partners() {
  return (
    <section id="partners" className="partners">
      <div className="container">
        <h2>ПАРТНЕРИ</h2>
        <div className="partners-logo">
          <img
            className="arr"
            src={arr}
            all="Агенція Регіонального Розвитку Львівщини"
          />
          <img
            className="ministry"
            src={ministry}
            alt="Міністерство У Справах Ветеранів України"
          />
          <img
            className="office"
            src={office}
            alt="Офіс Підтримки Родин Війсковослужбовців"
          />
        </div>
      </div>
    </section>
  );
}
