import "./style.scss";
import phone from "../../assets/icons/Phone.svg";
import vbh from "../../assets/logo/vbh.png";

export default function Header() {
  return (
    <header>
      <nav className="navbar navbar-expand-lg bg-transparent">
        <div className="container">
          <a className="navbar-brand" href="/">
            <img src={vbh} alt="logo" />
          </a>
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarNav"
            aria-controls="navbarNav"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarNav">
            <ul className="navbar-nav">
              <li className="nav-item">
                <a className="nav-link" href="/#about">
                  ПРО НАС
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="/#vectors">
                  НАПРЯМКИ
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="/#partners">
                  ПАРТНЕРИ
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="/vacancies">
                  ВАКАНСІЇ
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="/#form">
                  КОНТАКТИ
                </a>
              </li>
            </ul>
            <div className="number-container">
              <img src={phone} alt="logo" />
              <a href="tel:+38(097)0401893" className="number">
                +38(097) 040 18 93
              </a>
            </div>
          </div>
          <div className="number-container">
            <img src={phone} alt="logo" />
            <a href="tel:+38(097)0401893" className="number">
              +38(097) 040 18 93
            </a>
          </div>
        </div>
      </nav>
    </header>
  );
}
