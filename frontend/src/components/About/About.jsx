import "./style.scss";
import logo from "../../assets/logo/LOGO.svg";
import mission from "../../assets/icons/mission.png";

export default function About() {
  return (
    <section className="about" id="about">
      <div className="container">
        <div className="about-wrapper">
          <div className="about-heading">
            <img src={logo} />
            <h3>
              ВБХ — платформа для взаємодії і розвитку військовослужбовців
              (ветеранів) та їхніх сімей.
            </h3>
          </div>

          <div className="mission">
            <img className="mission-logo" src={mission} alt="місія" />
            <p>
              Місія: адаптація ветеранів до цивільного життя, створення
              сприятливих умов для військовослужбовців та їхніх сімей у пошуку
              роботи, започаткуванні і просуванні власної справи.
            </p>
          </div>
        </div>
      </div>
    </section>
  );
}
