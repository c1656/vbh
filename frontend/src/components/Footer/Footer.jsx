import "./style.scss";

import inst from "../../assets/icons/instagram.svg";
import tg from "../../assets/icons/telegram.svg";
import fb from "../../assets/icons/facebook.svg";
import mail from "../../assets/icons/mail.png";
import lin from "../../assets/icons/in.png";
import vbh from "../../assets/logo/vbh.png";

export default function Footer() {
  return (
    <footer className="footer" id="footer">
      <div className="container">
        <div className="row footer-border">
          <div className="col-12 col-md-12 col-lg-3 footer-logo-container">
            <a className="footer-logo" href="#">
              <img src={vbh} alt="logo" />
              <p>
                Ветеранський
                <br />
                Бізнес
                <br />
                Хаб
              </p>
            </a>
          </div>
          <div className="col-12 col-md-6 col-lg-3 address">
            <address>
              м.Львів,
              <br />
              Проспект Шевченка 7,
              <br />4 поверх, 25 кабінет
            </address>
          </div>
          <div className="col-12 col-md-6 col-lg-3 contact">
            <a href="mailto:veteranbizhub@gmail.com">veteranbizhub@gmail.com</a>
            <a href="tel:+38(097)0401893">+38(097) 040 18 93</a>
          </div>
          <div className="col-12 col-md-12 col-lg-3 contact">
            <div className="social-media">
              <a
                target="_blank"
                href="https://www.instagram.com/veteranbizhub/"
              >
                <img src={inst} />
              </a>
              <a target="_blank" href="https://t.me/veteranbizhub">
                <img src={tg} />
              </a>
              <a
                target="_blank"
                href="https://www.facebook.com/people/%D0%92%D0%B5%D1%82%D0%B5%D1%80%D0%B0%D0%BD%D1%81%D1%8C%D0%BA%D0%B8%D0%B9-%D0%91%D1%96%D0%B7%D0%BD%D0%B5%D1%81-%D0%A5%D0%B0%D0%B1/61552302695064/"
              >
                <img src={fb} />
              </a>
              <a href="mailto:veteranbizhub@gmail.com">
                <img src={mail} />
              </a>
              <a
                target="_blank"
                href="https://www.linkedin.com/in/veteranbizhub/"
              >
                <img src={lin} />
              </a>
            </div>
          </div>
        </div>
        <div className="row footer-bottom">
          <div className="col-12 col-md-6 copyright">
            <p className="text-muted">
              &copy; 2023 ГО "Ветеранський бізнес хаб". Всі права захищено.
            </p>
          </div>
          <div className="col col-md-6 developer">
            <a target="_blank" href="https://convalia.digital">
              convalia.digital
            </a>
          </div>
        </div>
      </div>
    </footer>
  );
}
