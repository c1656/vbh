import "./style.scss";
import inclusive from "../../assets/icons/inclusive.png";
import peace from "../../assets/icons/peace.png";
import search from "../../assets/icons/search.png";
import pack from "../../assets/icons/pack.png";

export default function Problems() {
  return (
    <section className="problems">
      <div className="container">
        <h2>Проблеми, що потребують рішень</h2>
        <div className="row">
          <div className="col-12 col-lg-6 problem">
            <div className="image-container">
              <img src={inclusive} alt="інклюзія" />
            </div>
            <p>
              Війна триває – кількість військовослужбовців/ ветеранів, зокрема з
              інклюзією, збільшується
            </p>
          </div>
          <div className="col-12 col-lg-6 problem">
            <div className="image-container">
              <img src={peace} alt="голуб миру" />
            </div>

            <p>
              Демобілізовані військові та їх сім’ї шукають підтримки в
              інтеграції до соціально-економічного життя в тилу
            </p>
          </div>
          <div className="col-12 col-lg-6 problem">
            <div className="image-container">
              <img src={search} alt="пошук кадрів" />
            </div>

            <p>Приватний сектор потребує закриття кадрових потреб ринку</p>
          </div>
          <div className="col-12 col-lg-6 problem">
            <div className="image-container">
              <img src={pack} alt="робота" />
            </div>

            <p>
              28% демобілізованих ветеранів, мають статус безробітного, 63,6%
              бажають мати власну справу, а 15,1% хочуть бути найманими
              працівниками
            </p>
          </div>
        </div>
      </div>
    </section>
  );
}
