export { default as Header } from "./Header/Header";
export { default as Main } from "./Main/Main";
export { default as Problems } from "./Problems/Problems";
export { default as About } from "./About/About";
export { default as Partners } from "./Partners/Partners";
export { default as Footer } from "./Footer/Footer";
export { default as Directions } from "./Directions/Directions";
export { default as Form } from "./Form/Form";
