import { BrowserRouter, Route, Routes } from "react-router-dom";
import { Header, Footer } from "./components";
import { Home, Vacancies, VacancyPage } from "./pages";
import "./styles/main.scss";

export default function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Header />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/vacancies" element={<Vacancies />} />
          <Route path="/vacancy/:id" element={<VacancyPage />} />
        </Routes>
        <Footer />
      </BrowserRouter>
    </div>
  );
}
